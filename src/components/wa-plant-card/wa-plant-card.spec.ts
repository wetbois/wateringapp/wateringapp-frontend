import {shallowMount} from '@vue/test-utils';
import WaPlantCard from './wa-plant-card.vue';

import {PlantInfo} from 'src/domain/plant-info';
import {byTestId} from 'test/utils';

const defaultPlantInfo = PlantInfo({
	id: 1,
	nickName: 'firstNickName',
	speciesName: 'firstSpeciesName',
	imageUrl: 'http://some.path.to/an/image',
	waterPercentage: 50,
	nextWateringDate: new Date(),
	waterQuantity: 200,
});

const testIds = {
	image: 'image',
	nickName: 'plant-nickname',
	speciesName: 'plant-species-name',
	progress: 'plant-watering-progress',
	calendar: 'calendar-icon',
	water: 'water-icon',
	edit: 'edit-icon',
}

describe('Default display', () => {
	test('Displays all necessary parts given as props', () => {
		/*const wrapper = setup();
		expect(wrapper.find(byTestId(testIds.nickName)).text()).toContain(defaultPlantInfo.nickName);
		expect(wrapper.find(byTestId(testIds.speciesName)).text()).toContain(defaultPlantInfo.speciesName);

		expect(wrapper.find(byTestId(testIds.image)).attributes()['src']).toStrictEqual(defaultPlantInfo.imageUrl);
		expect(wrapper.find(byTestId(testIds.progress)).attributes()['value']).toStrictEqual(defaultPlantInfo.waterPercentage.toString());

		expect(wrapper.find(byTestId(testIds.calendar)).exists()).toBeTruthy();
		expect(wrapper.find(byTestId(testIds.water)).exists()).toBeTruthy();
		expect(wrapper.find(byTestId(testIds.edit)).exists()).toBeTruthy();*/
	});
});

const setup = (plantInfo = defaultPlantInfo) => {
	return shallowMount(WaPlantCard, {props: {plantInfo: defaultPlantInfo}});
}
