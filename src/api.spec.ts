import { Api } from './api';
import { AxiosMock } from "test/utils";
import { PlantInfo } from "src/domain/plant-info";

const firstMockPlant = PlantInfo({
	id: 1,
	nickName: 'firstNickName',
	speciesName: 'firstSpeciesName',
	imageUrl: 'http://some.path.to/an/image',
	waterPercentage: 50,
	nextWateringDate: new Date(),
	waterQuantity: 200,
});

const secondMockPlant = PlantInfo({
	id: 2,
	nickName: 'secondNickName',
	speciesName: 'secondSpeciesName',
	imageUrl: 'http://some.path.to/an/image',
	waterPercentage: 25,
	nextWateringDate: new Date(),
	waterQuantity: 100,
});

const plantsArray = [firstMockPlant, secondMockPlant];

describe('gatAllPlants - ', () => {
	beforeEach(() => {
		jest.clearAllMocks();
	});

	test('If backend returns all plants, it sends back response', async () => {
		const api = Api({ ...AxiosMock(), ...{
				get: jest.fn().mockReturnValue(
					new Promise(
						resolve => resolve({ data: plantsArray} )
					)
				)
			}} );

		const result = await api.getAllPlants();
		expect(result).toStrictEqual(plantsArray);
	});

	test('If backend rejects with 401, throws specified error', async () => {
		const api = Api({ ...AxiosMock(), ...{
				get: jest.fn().mockRejectedValue({ response: { status: 401 } })
			}});

		let errorMessage = '';
		try {
			await api.getPlantById(1);
		} catch ({ message }) {
			// @ts-ignore
			errorMessage = message;
		}
		expect(errorMessage).toStrictEqual('notifications.global.unauthorized');
	});

	test('If backend rejects with other error, throws default error', async () => {
		const api = Api({ ...AxiosMock(), ...{
				get: jest.fn().mockRejectedValue({ response: { status: 418 } })
			}});

		let errorMessage = '';
		try {
			await api.getPlantById(1);
		} catch ({ message }) {
			// @ts-ignore
			errorMessage = message;
		}
		expect(errorMessage).toStrictEqual('notifications.defaultError');
	});
})

describe('getPlantById - ', () => {
	beforeEach(() => {
		jest.clearAllMocks();
	});

	test('If backend returns a plant, it sends back response', async () => {
		const api = Api({ ...AxiosMock(), ...{
			get: jest.fn().mockReturnValue(
					new Promise(
				resolve => resolve({ data: firstMockPlant} )
					)
			)
		}} );

		const result = await api.getPlantById(1);
		expect(result).toStrictEqual(firstMockPlant);
	});

	test('If backend rejects with 401, throws specified error', async () => {
		const api = Api({ ...AxiosMock(), ...{
			get: jest.fn().mockRejectedValue({ response: { status: 401 } })
		}});

		let errorMessage = '';
		try {
			await api.getPlantById(1);
		} catch ({ message }) {
			// @ts-ignore
			errorMessage = message;
		}
		expect(errorMessage).toStrictEqual('notifications.global.unauthorized');
	});

	test('If backend rejects with other error, throws default error', async () => {
		const api = Api({ ...AxiosMock(), ...{
			get: jest.fn().mockRejectedValue({ response: { status: 418 } })
		}});

		let errorMessage = '';
		try {
			await api.getPlantById(1);
		} catch ({ message }) {
			// @ts-ignore
			errorMessage = message;
		}
		expect(errorMessage).toStrictEqual('notifications.defaultError');
	});
});
