import {ComponentObjectPropsOptions, defineComponent, h, onMounted, onUnmounted, ref, Slots, VNode} from 'vue';

/**
 * Converts a web component to a Vue component
 *
 * Incubated here for now. Once it's API is stable the intention is to open-source it.
 * */
export function toVueComponent(CustomElement: CustomElementConstructor, {
	name = CustomElement.name,
	props,
	class: className = hyphenate(name),
	allowUndefinedProps = false,
	mapEvents = [],
}: ToVueComponentOptions = {}) {
	const { scopedTag, ScopedElement } = _initElement(CustomElement, name);
	const _props = props ?? toVuePropOptions(customPropsOf(ScopedElement));
	const filterProps = filterObject(isAllowedProp(allowUndefinedProps));

	return defineComponent({
		name,
		props: _props,
		setup: (props, { emit, slots }) => {
			const listeners = _convertPropUpdateEvents({ props, emit });
			const { el } = _useEventMapping({ emit, mapEvents });
			return () => {
				const domSlots = toDomSlots(slots);
				const properties = filterProps(props);
				return h(scopedTag, { ...properties, ...listeners, ref: el, class: className }, domSlots);
			};
		},
	});
}

type ToVueComponentOptions = {
	/** Name of the Vue component in CamelCase format */
	name?: string;
	/** Additional class(es) on the wrapped component for theming. The kebab-case name by default. */
	class?: string;
	/** Props of the resulting Vue component. Defaults to all custom props with a dynamic get and set. */
	props?: ComponentObjectPropsOptions;
	/** Explicitly allow some or all props to pass undefined values to the wrapped component. False by default. */
	allowUndefinedProps?: string[] | boolean;
	/* Allow mapping DOM events to Vue emits */
	mapEvents?: EventMapping[];
}

type EventMapping = {
	event: string;
	emit: string;
	map: (event: Event) => unknown;
}

/** Ensures the custom element is ready to be wrapped by sub-classing, registering, and triggering any lazy init */
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export function _initElement(CustomElement: CustomElementConstructor, name: string): { scopedTag: string; ScopedElement: any } {
	class ScopedElement extends CustomElement {}
	/* Register custom element with a scoped tag */
	const scopedTag = toUniqueTag(hyphenate(name));
	customElements.define(scopedTag, ScopedElement);
	/** The scoped custom elements polyfill swaps out the registry seemingly removing registered entries */
	setTimeout(() => ensureRegistered(scopedTag, ScopedElement));
	/* Trigger any init hooks for example LitElement uses that may mutate the element prototype chain */
	document.createElement(scopedTag);
	return { scopedTag, ScopedElement };
}

/**
 * Returns custom properties declared on top of a webcomponent.
 * A prop is recognized by having both a dynamic get and set method.
 * */
function customPropsOf(Constructor: CustomElementConstructor): string[] {
	if (!Constructor.prototype || HTMLElement === Constructor || Constructor.name === 'HTMLElement') { return []; }
	const hasGetAndSet = ([/**/, { get, set }]: [string, PropertyDescriptor]) => !!get && !!set;
	const isPublic = (prop: string) => !prop.startsWith('_');
	const props = Object.entries(Object.getOwnPropertyDescriptors(Constructor.prototype))
			.filter(hasGetAndSet)
			.map(([prop]) => prop)
			.filter(isPublic);
	return [...props, ...customPropsOf(Object.getPrototypeOf(Constructor))];
}

function ensureRegistered(tag: string, Element: CustomElementConstructor) {
	if (customElements.get(tag)) { return; }
	customElements.define(tag, Element);
}

function toVuePropOptions(props: string[]) {
	return Object.fromEntries(props.map(prop => [prop, {}]));
}

function isAllowedProp(allowUndefinedProps?: string[] | boolean) {
	return (prop: string, value: unknown) => {
		if (value !== undefined) { return true; }
		if (!Array.isArray(allowUndefinedProps)) { return !!allowUndefinedProps; }
		return allowUndefinedProps.includes(prop);
	};
}

/**
 * Returns native DOM Event handlers for all onUpdate[PROP_NAME] events
 * that cancel the native event and instead re-emit the detail via the Vue API.
 * Essentially, this adds support for the v-model shorthand.
 * */
export type Emit = (event: string, ...args: unknown[]) => void;
export function _convertPropUpdateEvents(
	{ props, emit }:
	{ props: Record<string, unknown>; emit: Emit },
) {
	const toEventName = (prop: string) => `onUpdate${capitalize(prop)}`;
	const toEventHandler = (prop: string) => (event: CustomEvent) => {
		if (event.detail === undefined) { return; }
		event.preventDefault();
		const value = Array.isArray(event.detail) ? event.detail : [event.detail];
		emit(`update:${prop}`, ...value);
	};
	return Object.fromEntries(Object.keys(props)
			.map(prop => [
				toEventName(prop),
				toEventHandler(prop),
			]));
}

export function _useEventMapping(
	{ emit, mapEvents }:
	{ emit: Emit; mapEvents: EventMapping[] },
) {
	const el = ref<HTMLElement>();
	for (const mapping of mapEvents) {
		const listener = toListener({ emit, mapping });
		onMounted(() => el.value?.addEventListener(mapping.event, listener));
		onUnmounted(() => el.value?.removeEventListener(mapping.event, listener));
	}
	return { el };
}

function toListener({ emit, mapping }: { emit: Emit; mapping: EventMapping }) {
	return (event: Event) => emit(mapping.emit, mapping.map(event));
}

export function targetProp(name: string) {
	return ({ target }: Event) => {
		const element = target as unknown as Record<string, unknown>;
		return element[name];
	};
}

function toDomSlots(slots: Slots) {
	return Object.entries(slots)
			.flatMap(([slot, nodes]) => {
				if (nodes === undefined) { return []; }
				if (slot === 'default') { return nodes(); }
				return nodes().map(withSlot(slot));
			});
}

function withSlot(slot: string): <T extends VNode>(vnode: T) => T {
	return vnode => ({ ...vnode, props: { ...vnode.props, slot } });
}

/** Behavior matches @open-wc/scoped-elements v1  */
let counter = Math.round(Math.random() * 100000);
function toUniqueTag(tag: string) {
	return `${tag}-${counter++}`;
}

function hyphenate(string: string) {
	const hyphenateRE = /\B([A-Z])/g;
	return string.replace(hyphenateRE, '-$1').toLowerCase();
}

function filterObject(predicate: (prop: string, value: unknown) => boolean) {
	return (object: Record<string, unknown>) => Object.fromEntries(Object.entries(object)
			.filter(([prop, value]) => predicate(prop, value)));
}

function capitalize(str: string) {
	const [first, ...letters] = str;
	return `${first.toUpperCase()}${letters.join('')}`;
}
