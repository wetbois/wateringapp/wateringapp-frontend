import axios, { AxiosInstance } from 'axios';
import { PlantInfo } from 'src/domain/plant-info';

export type AxiosPartial = Pick<AxiosInstance, 'get'|'post'|'put'|'delete'>;

export function Api({ get }: AxiosPartial = Axios()) {
	return {
		async getAllPlants(): Promise<Array<PlantInfo>> {
			try {
				const { data } = await get<Array<PlantInfo>>('/plants/all');
				return data.map(PlantInfo);
			} catch ({ response }) {
				// @ts-ignore
				if (response.status === 401) {
					throw new Error('notifications.global.unauthorized');
				}
				throw new Error('notifications.defaultError');
			}
		},
		async getPlantById(plantId: number): Promise<PlantInfo> {
			try {
				const { data } = await get<PlantInfo>(`/plants/${plantId}`);
				return PlantInfo(data);
					// return new Promise(
					// 		(resolve => {
					// 				setTimeout(() => {
					// 						resolve(PlantInfo({
					// 								id: 1,
					// 								nickName: 'Az én anyósom',
					// 								speciesName: 'Sansaveria',
					// 								imageUrl: 'https://citygreen.hu/wp-content/uploads/2017/01/Sansevieria-trifasciata.jpg',
					// 								waterPercentage: 30,
					// 								nextWateringDate: (new Date(2022, 6, 19)),
					// 								waterQuantity: 200,
					// 						}))
					// 				}, 2000);
					// 		})
					// );
			} catch ({ response }) {
				// @ts-ignore
				if (response.status === 401) {
					throw new Error('notifications.global.unauthorized');
				}
				throw new Error('notifications.defaultError');
			}
		},
	};
}
export type Api = ReturnType<typeof Api>;

function Axios() {
	return axios.create({ baseURL: process.env.API_PATH });
}
