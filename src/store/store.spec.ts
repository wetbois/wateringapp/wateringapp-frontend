import { Api } from 'src/api';
import { PlantInfo } from 'src/domain/plant-info';
import {NotificationType, Store} from 'src/store/store';

const createMockApiImplementation = (successReturnValue: unknown = undefined, errorReturnMessage = '') => {
	return jest.fn().mockImplementation(() => {
		if (errorReturnMessage) {
			throw new Error(errorReturnMessage);
		}
		return new Promise(resolve => resolve(successReturnValue));
	});
};

const commit = jest.fn();
const dispatch = jest.fn();

const defaultPlantToReturn = PlantInfo({
	id: 1,
	nickName: 'Az én anyósom',
	speciesName: 'Sansaveria',
	imageUrl: 'https://citygreen.hu/wp-content/uploads/2017/01/Sansevieria-trifasciata.jpg',
	waterPercentage: 30,
	nextWateringDate: (new Date(2022, 6, 19)),
	waterQuantity: 200,
});

const anotherPlantToReturn = PlantInfo({
	id: 2,
	nickName: 'Az én anyósom',
	speciesName: 'Sansaveria',
	imageUrl: 'https://citygreen.hu/wp-content/uploads/2017/01/Sansevieria-trifasciata.jpg',
	waterPercentage: 25,
	nextWateringDate: (new Date(2022, 6, 19)),
	waterQuantity: 100,
});

const plantsArrayToReturn = [defaultPlantToReturn, anotherPlantToReturn];

const createMockApi = (mockedApiImplementationPoints: Partial<Api> = {}): Api => {
	return {
		getPlantById: createMockApiImplementation(PlantInfo(defaultPlantToReturn)),
		getAllPlants: createMockApiImplementation(plantsArrayToReturn),
		...mockedApiImplementationPoints,
	}
};

const setupStore = (api: Api = createMockApi()) => {
	return { api, store: Store({ api }) };
};

describe('mutations', () => {
	let { store } = setupStore();
	beforeEach(() => {
		jest.clearAllMocks();
		({ store } = setupStore());
	});

	test('updatePlantList', () => {
		expect(store.state.plantList).toStrictEqual([]);

		store.mutations.updatePlantList(store.state, [defaultPlantToReturn]);

		expect(store.state.plantList).toStrictEqual([defaultPlantToReturn]);
	});

	test('notifyAboutSuccessfulLoad', () => {
		expect(store.state.plantList).toStrictEqual([]);

		store.mutations.notifyAboutSuccessfulLoad(store.state);

		expect(store.state.isLoaded).toBeTruthy();
	});
});

describe('actions', () => {
	beforeEach(jest.clearAllMocks);

	describe('getAllPlant', () => {
		test('When api returns all plants, save it to the state', async () => {
			const { api, store } = setupStore();

			await store.actions.getAllPlants(
				{ commit, dispatch },
			);

			expect(api.getAllPlants).toHaveBeenCalled();
			expect(commit).toHaveBeenCalledTimes(2);
			expect(commit).toHaveBeenCalledWith('updatePlantList', plantsArrayToReturn);
			expect(commit).toHaveBeenCalledWith('notifyAboutSuccessfulLoad');
		});

		test('When api throws error, it is thrown forward to notification widget', async () => {
			const mockErrorMessage = 'very.unique.error.message';
			const { api, store } = setupStore(createMockApi({
				getAllPlants: jest.fn().mockImplementation( () => { throw new Error(mockErrorMessage)})
			}));

			await store.actions.getAllPlants(
				{ commit, dispatch }
			);

			expect(api.getAllPlants).toHaveBeenCalled();
			expect(dispatch).toHaveBeenCalledWith(
				'popNotification',
				{ message: mockErrorMessage, type: NotificationType.ERROR },
			)
		});
	});

	describe('getPlant', () => {
		test('When api returns a plant, save it to the state', async () => {
			const { api, store } = setupStore();

			await store.actions.getPlant(
					{ commit, dispatch },
					defaultPlantToReturn.id,
				);

			expect(api.getPlantById).toHaveBeenCalledWith(defaultPlantToReturn.id);
			expect(commit).toHaveBeenCalledTimes(2);
			expect(commit).toHaveBeenCalledWith('updatePlantList', [defaultPlantToReturn]);
			expect(commit).toHaveBeenCalledWith('notifyAboutSuccessfulLoad');
		});

		test('When api throws error, it is thrown forward to notification widget', async () => {
			const mockErrorMessage = 'very.unique.error.message';
			const { api, store } = setupStore(createMockApi({
				getPlantById: jest.fn().mockImplementation( () => { throw new Error(mockErrorMessage)})
			}));

			await store.actions.getPlant(
				{ commit, dispatch },
				defaultPlantToReturn.id,
			);

			expect(api.getPlantById).toHaveBeenCalledWith(defaultPlantToReturn.id);
			expect(dispatch).toHaveBeenCalledWith(
				'popNotification',
				{ message: mockErrorMessage, type: NotificationType.ERROR },
			)
		});
	});
});
