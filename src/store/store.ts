import { Api } from '../api';
import { PlantInfo } from 'src/domain/plant-info';
import { Context } from 'vm';

export enum NotificationType {
	SUCCESS,
	INFO,
	WARNING,
	ERROR,
}

export type Notification = {
	display: boolean;
	message: string;
	type: NotificationType
}

export type State = {
	plantList: Array<PlantInfo>;
	notification: Notification;
		isLoaded: boolean;
}
// This structure follows the Vuex store specification
export function Store({ api }: { api: Api }) {
	// The most relevant object to your app
	const state: State = {
		plantList: [],
		notification: {
			display: false,
			message: '',
			type: NotificationType.INFO,
		},
		isLoaded: false,
	};


	const mutations = {
		updatePlantList(state: State, plantList: Array<PlantInfo>) {
			state.plantList = plantList;
		},
		updateNotification(state: State, notificationInfo: Notification) {
			state.notification = {
				...state.notification,
				...notificationInfo
			};
		},
		notifyAboutSuccessfulLoad(state: State) {
			state.isLoaded = true;
		}
	};

	const getters = { };

	const actions = {
		popNotification: ({ commit }: Pick<Context, 'commit'>, notificationInfo: Omit<Notification, 'display'>) => {
			commit('updateNotification', {
				...notificationInfo,
				display: true,
			});
			setTimeout(() => {
				commit('updateNotification', { display: false });
			}, 4000);
		},
		clearNotification({ commit }: Pick<Context, 'commit'>) {
			commit('updateNotification',  { display: false });
		},
		async getAllPlants({ commit, dispatch }: Pick<Context, 'commit' | 'dispatch'>) {
			try {
				const plants = await api.getAllPlants();
				commit('updatePlantList', plants);
				commit('notifyAboutSuccessfulLoad');
			} catch ({ message }) {
				dispatch('popNotification', { message, type: NotificationType.ERROR});
			}
		},
		async getPlant({ commit, dispatch }: Pick<Context, 'commit' | 'dispatch'>, plantId: number) {
			try {
				const plant = await api.getPlantById(plantId);
				commit('updatePlantList', [plant]);
				commit('notifyAboutSuccessfulLoad');
			} catch ({ message }) {
				dispatch('popNotification', { message, type: NotificationType.ERROR});
			}
		},
	};

	return { state, mutations, getters, actions };
}
export type Store = ReturnType<typeof Store>;
