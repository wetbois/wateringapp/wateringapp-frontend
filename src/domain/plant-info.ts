export type PlantInfo = {
	id: number;
	waterQuantity: number;
	speciesName: string;
	imageUrl: string;
	nickName: string;
	nextWateringDate: Date;
	waterPercentage: number;
}

export function PlantInfo({
							  id = 0,
							  nickName = '',
							  speciesName = '',
							  imageUrl = '',
							  waterPercentage = 0,
							  nextWateringDate = new Date(),
							  waterQuantity = 0, // in cl
						  }: PlantInfo) : PlantInfo {
	return { id, nickName, speciesName, imageUrl, waterPercentage, nextWateringDate, waterQuantity };
}

